const container = document.querySelector(".container");

const number1 = document.querySelector("#number1");
const number2 = document.querySelector("#number2");
const number3 = document.querySelector("#number3");
const number4 = document.querySelector("#number4");

const nextStep1 = document.querySelector("#step-1-button");
const nextStep2 = document.querySelector("#step-2-button");
const nextStep3 = document.querySelector("#step-3-button");
const nextStep4 = document.querySelector("#step-4-button");

const goBack1 = document.querySelector("#go-back-1");
const goBack2 = document.querySelector("#go-back-2");
const goBack3 = document.querySelector("#go-back-3");

const step1 = document.querySelector("#step-1");
const step2 = document.querySelector("#step-2");
const step3 = document.querySelector("#step-3");
const step4 = document.querySelector("#step-4");
const step5 = document.querySelector("#step-5");



nextStep1.addEventListener("click", function () {

  const nameVal = document.querySelector("#name").value;
  const mail = document.querySelector("#mail").value;
  const phone = document.querySelector("#phone").value;

if(nameVal.trim() === ''){
  document.querySelector("#name-warn").textContent = "Enter Name";
} else if (!isNameValid(nameVal)){
  document.querySelector("#name-warn").textContent = "Name must include only alphabets";
} else {
  document.querySelector("#name-warn").textContent = "";
}

if(mail.trim() === ''){
  document.querySelector("#mail-warn").textContent = "Enter Email Address";
} else if (!isMailValid(mail)){
  document.querySelector("#mail-warn").textContent = "Email must include name@gmail.com";
} else {
  document.querySelector("#mail-warn").textContent = "";
}

if(phone.trim() === ''){
  document.querySelector("#phone-warn").textContent = "Enter Phone Number";
} else if (!isPhoneValid(phone)){
  document.querySelector("#phone-warn").textContent = "Phone must include only numbers";
} else {
  document.querySelector("#phone-warn").textContent = "";
}

  if ( nameVal.trim() !== '' && mail.trim() !== '' && phone.trim() !== '') {
    if (isNameValid(nameVal) &&isMailValid(mail) && isPhoneValid(phone)) {
      step1.style.display = "none";
      step2.style.display = "flex";
      // console.log(number1,number2, number2.style.backgroundColor);
      number1.style.backgroundColor = "blue";
      number1.children[0].style.color = "white";
     number2.style.backgroundColor = "hsl(206, 94%, 87%)";
     number2.children[0].style.color = "hsl(213, 96%, 18%)";

    }
  }

});

function isNameValid(input) {
  const lettersRegex = /^[A-Za-z]+$/;
  return lettersRegex.test(input);
}

function isMailValid(email) {
  const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailPattern.test(email);
}

function isPhoneValid(phoneNumber) {
  const phonePattern = /^\d{10}$/;
  return phonePattern.test(phoneNumber);
}


if(document.querySelector("#toggle").checked){

  document.querySelectorAll(".monthly").forEach((item) => {
    item.style.display = "none";
  })
  document.querySelectorAll(".yearly").forEach((item) => {
    item.style.display = "block";
  })

 } else {

    document.querySelectorAll(".monthly").forEach((item) => {
    item.style.display = "block";
  })
  document.querySelectorAll(".yearly").forEach((item) => {
    item.style.display = "none";
  })

 }

// monthly and yearly toggling

document.querySelector("#toggle").addEventListener("change", function (event){

//  console.log(event.target.checked);

 if(event.target.checked){

  document.querySelectorAll(".monthly").forEach((item) => {
    item.style.display = "none";
  })
  document.querySelectorAll(".yearly").forEach((item) => {
    item.style.display = "block";
  })

 } else {

    document.querySelectorAll(".monthly").forEach((item) => {
    item.style.display = "block";
  })
  document.querySelectorAll(".yearly").forEach((item) => {
    item.style.display = "none";
  })

 }

})


// Step-2 button

let plans = '';
let addOns = [];

nextStep2.addEventListener("click", function(){
if(plans === ''){
document.querySelector(".plan-warn").textContent ="You have no selected plan, you must select one plan to move forward"
} else {
  step2.style.display = "none";
  step3.style.display = "flex";
  number2.style.backgroundColor = "blue";
  number2.children[0].style.color = "white";
 number3.style.backgroundColor = "hsl(206, 94%, 87%)";
 number3.children[0].style.color = "hsl(213, 96%, 18%)";
}
})


// Step-2 Event Listener

step2.addEventListener("click", function(event){


 if(event.target.classList.contains("plans")){
  document.querySelectorAll(".blue-border").forEach((item) => {
    item.classList.remove("blue-border");
  })
  
  event.target.classList.add("blue-border");
   plans = event.target;
 } else if (event.target.parentNode.classList.contains("plans")){
  document.querySelectorAll(".blue-border").forEach((item) => {
    item.classList.remove("blue-border");
  })
  event.target.parentNode.classList.add("blue-border");
  plans = event.target.parentNode;
 } else if (event.target.parentNode.parentNode.classList.contains("plans")){
  document.querySelectorAll(".blue-border").forEach((item) => {
    item.classList.remove("blue-border");
  })
  event.target.parentNode.parentNode.classList.add("blue-border");
  plans = event.target.parentNode.parentNode;
 }
// console.log(plans)
})

let checked = false;

// Step 3 Add Event Listener

step3.addEventListener("click", function(event){


  if(event.target.classList.contains("check")){
    // event.target.getElementByTagName("input")
    checked= !checked;
    event.target.children[0].children[0].children[0].checked = checked;
   
  } else if(event.target.parentNode.classList.contains("check")){
    checked= !checked;
    event.target.parentNode.children[0].children[0].children[0].checked = checked;
    // event.target.parentNode.children[0].children[0].children[0].checked = checked;
   
  } else if(event.target.parentNode.parentNode.classList.contains("check")){
    checked= !checked;
    event.target.parentNode.parentNode.children[0].children[0].children[0].checked = checked;

  } else if(event.target.parentNode.parentNode.parentNode.classList.contains("check")){
    checked= !checked;
    event.target.parentNode.parentNode.parentNode.children[0].children[0].children[0].checked = checked;
     }
})



// nextSTep Event Listener 

nextStep3.addEventListener("click", function () {

  let total = 0;

const onlineCheck = document.querySelector("#online-service");
const storageCheck = document.querySelector("#large-storage");
const profileCheck = document.querySelector("#profile");

  if(onlineCheck.checked){
    if(document.querySelector("#toggle").checked){
      addOns.push({service:onlineCheck.parentNode.parentNode.children[1].children[0].textContent, price:onlineCheck.parentNode.parentNode.parentNode.children[2].textContent})
    } else {
      addOns.push({service:onlineCheck.parentNode.parentNode.children[1].children[0].textContent, price:onlineCheck.parentNode.parentNode.parentNode.children[1].textContent})
    }
  }
  if(storageCheck.checked){
    if(document.querySelector("#toggle").checked){
      addOns.push({service:storageCheck.parentNode.parentNode.children[1].children[0].textContent, price:storageCheck.parentNode.parentNode.parentNode.children[2].textContent})
    } else {
      addOns.push({service:storageCheck.parentNode.parentNode.children[1].children[0].textContent, price:storageCheck.parentNode.parentNode.parentNode.children[1].textContent})
    }  }
  if(profileCheck.checked){
    if(document.querySelector("#toggle").checked){
      addOns.push({service:profileCheck.parentNode.parentNode.children[1].children[0].textContent, price:profileCheck.parentNode.parentNode.parentNode.children[2].textContent})
    } else {
      addOns.push({service:profileCheck.parentNode.parentNode.children[1].children[0].textContent, price:profileCheck.parentNode.parentNode.parentNode.children[1].textContent})
    }  
  }
 
 if (document.querySelector("#toggle").checked){
  document.querySelector("#plan").textContent = plans.children[2].children[0].textContent + `(yearly)`;
  document.querySelector("#value").textContent = plans.children[2].children[1].textContent;
  
  total += +plans.children[2].children[1].textContent.replace("$",'').replace("yr","").replace("/",'');
  // console.log(plans.children[2].children[1].textContent,total)
 } else {
  document.querySelector("#plan").textContent = plans.children[1].children[0].textContent + `(monthly)`;;
  document.querySelector("#value").textContent = plans.children[1].children[1].textContent;
  total += +plans.children[1].children[1].textContent.replace("$",'').replace("mo","").replace("/",'');
  // console.log(plans.children[1].children[1].textContent, total)
 }

addOns.forEach((item) => {

  const div1 = document.createElement("div");
  const div2 = document.createElement("div");
  const div3 = document.createElement("div");
  div1.classList.add("flex-row");
  div1.classList.add("space-between");
  div2.textContent = item.service;
  div3.textContent = item.price;
  // console.log(total);
  if(document.querySelector("#toggle").checked){
    total += +item.price.replace("$",'').replace("yr","").replace("/",'').replace("+",'');

  } else {
    total += +item.price.replace("$",'').replace("mo","").replace("/",'').replace("+",'');

  }

  div1.appendChild(div2);
  div1.appendChild(div3);

  document.querySelector("#service-details").appendChild(div1)
})

if (document.querySelector("#toggle").checked){
  document.querySelector("#total").textContent = '$'+ total + '/year'
} else {
  document.querySelector("#total").textContent = '$'+ total + '/month'

}


  step3.style.display = "none";
  step4.style.display = "flex";

  number3.style.backgroundColor = "blue";
  number3.children[0].style.color = "white";
 number4.style.backgroundColor = "hsl(206, 94%, 87%)";
 number4.children[0].style.color = "hsl(213, 96%, 18%)";
})

// nextStep 4 
nextStep4.addEventListener("click", function() {
  step4.style.display = "none";
  step5.style.display = "contents";
 
  number4.style.backgroundColor = "blue";
  number4.children[0].style.color = "white";
} )

goBack1.addEventListener("click", function(){
  step1.style.display = "block";
  step2.style.display = "none";

  number2.style.backgroundColor = "blue";
  number2.children[0].style.color = "white";
 number1.style.backgroundColor = "hsl(206, 94%, 87%)";
 number1.children[0].style.color = "hsl(213, 96%, 18%)";
})

goBack2.addEventListener("click", function(){
  // console.log(plans)
 
  plans.classList.add("blue-border");
  step2.style.display = "flex";
  step3.style.display = "none";

  number3.style.backgroundColor = "blue";
  number3.children[0].style.color = "white";
 number2.style.backgroundColor = "hsl(206, 94%, 87%)";
 number2.children[0].style.color = "hsl(213, 96%, 18%)";
})


goBack3.addEventListener("click", function(){

  document.querySelector("#service-details").innerHTML = "";
  addOns=[];
  step3.style.display = "flex";
  step4.style.display = "none";

  number4.style.backgroundColor = "blue";
  number4.children[0].style.color = "white";
 number3.style.backgroundColor = "hsl(206, 94%, 87%)";
 number3.children[0].style.color = "hsl(213, 96%, 18%)";
})



// change add eventlistener

document.querySelector("#change").addEventListener("click", function(){

  document.querySelector(".plan-warn").textContent  = '';
  document.querySelector("#service-details").innerHTML = "";
  addOns=[];

  step2.style.display = "flex";
  step4.style.display = "none";

  number4.style.backgroundColor = "blue";
  number4.children[0].style.color = "white";
 number2.style.backgroundColor = "hsl(206, 94%, 87%)";
 number2.children[0].style.color = "hsl(213, 96%, 18%)";
})